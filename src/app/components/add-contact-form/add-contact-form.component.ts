import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { Contact } from 'src/app/model/contact';
import { ContactsService } from 'src/app/services/contacts.service';


@Component({
  selector: 'app-add-contact-form',
  template: `
      <div class="form">
      <dl>
          <dt><label for="contact-name"><b>NAME</b></label></dt>
          <dd><input type="text" id="contact-name" [(ngModel)]="contact.name"></dd>

          <dt><label for="contact-type"><b>TYPE</b></label></dt>
          <dd>
              <select id="contact-type" [(ngModel)]="contact.type">
                  <option>phone</option>
                  <option>email</option>
              </select>
          </dd>

          <dt><label for="contact-value"><b>VAULE</b></label></dt>
          <dd><input type="text" id="contact-value" [(ngModel)]="contact.value"></dd>

          <div *ngIf="!contact.name||!contact.type||!contact.value">Пожалуйста заполните все поля</div>
          <dt><button *ngIf="contact.name&&contact.type&&contact.value" (click)="addContact()"><b>ADD</b></button></dt>
      </dl>
    </div>
  `,
  styleUrls: ['./add-contact-form.component.css']
})
export class AddContactFormComponent {
    public contact:Contact = new Contact('','',''); 

    constructor(@Inject(ContactsService) private contactServices:ContactsService){}


    addContact(){
        this.contactServices.addContact(this.contact);
        this.contact = new Contact('','','');
    }
}
