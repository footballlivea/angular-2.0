import { Component, Inject, Input, OnInit } from '@angular/core';
import { Contact } from 'src/app/model/contact';
import { ContactsService } from 'src/app/services/contacts.service';


@Component({
  selector: 'app-active-contact',
  template: `
      <div class="contact" *ngIf="contact!==null">
      <dt class="name">{{contact.name}}</dt>
      <hr width="100%" align="center" size="1px" color="white">
      <dd class="type">{{contact.type}}</dd>
      <dd class="value">{{contact.value}}</dd>           
      </div>
  `,
  styleUrls: ['./active-contact.component.css']
})
export class ActiveContactComponent {

    public contact:Contact|null = null; 

    constructor(@Inject(ContactsService) private contactServices:ContactsService){
      contactServices.getActiveContact().subscribe(c=>this.contact=c);
    }
}
