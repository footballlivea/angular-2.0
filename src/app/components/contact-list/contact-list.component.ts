import { Component, Inject, OnInit } from '@angular/core';
import { Contact } from 'src/app/model/contact';
import { ContactsService } from 'src/app/services/contacts.service';

@Component({
  selector: 'app-contact-list',
  template:`
  <ul>
  <div *ngFor="let contact of contacts">
      <li *ngIf="contact.name&&contact.type&&contact.value" (click)="choseContact(contact)" [class.active]="activeContact==contact">{{contact.name}}
      <button (click)="deleteContact(contact)">&#10006;</button>
      </li>
  </div>         
</ul>
  `,
  styleUrls: ['./contact-list.component.css']
})
export class ContactListComponent {

  activeContact:any;
  contacts:Contact[] = [];

  constructor(@Inject(ContactsService) private contactServices:ContactsService){
    contactServices.getContacts().subscribe(c=>this.contacts=c);
    contactServices.getActiveContact().subscribe(c=>this.activeContact=c);
  }

  choseContact(contact:any){
    this.activeContact = contact;
    this.contactServices.activeContact(contact);
  }

  deleteContact(contact:any){
    this.contactServices.deleteContact(contact)
  }
  
}
