import { Injectable } from '@angular/core';
import { merge, Observable, of, Subject } from 'rxjs';
import { Contact } from '../model/contact';

@Injectable({
  providedIn: 'root'
})
export class ContactsService {
  private contacts:Contact[] = [];
  public active:Contact|null = null;
  private contactsSubject:Subject<Contact[]> = new Subject<Contact[]>()
  private activeContactsSubject:Subject<Contact|null> = new Subject()

  public getContacts():Observable<Contact[]>{
    return merge(this.contactsSubject,of(this.contacts));
  }

  public addContact(contact:Contact){
    this.contacts.push(contact);
    this.contactsSubject.next(this.contacts)
  }

  public activeContact(contact:Contact|null){
    this.active=contact;
    this.activeContactsSubject.next(contact)
  }

  public getActiveContact():Observable<Contact|null>{
    return this.activeContactsSubject;
  }

  public deleteContact(contact:Contact){
    if(this.active==contact) this.activeContact(null);
    this.contacts=this.contacts.filter(c=>c!==contact);
    this.contactsSubject.next(this.contacts);

  }
}
