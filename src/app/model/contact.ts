export class Contact{

    constructor(
        public name:string, 
        public type:string, 
        public value:string
    ){}

    clone():Contact {
        return new Contact(this.name,this.type,this.value)
    }

}